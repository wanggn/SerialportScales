package com.bec.serialportlibrary;


/**
 * Created by lzp on 2018/5/23 0023.
 */
public interface SerialPortController {

    void openSerialPortAndReadData() throws RuntimeException;

    void closeSerialPort();

    void sendSerialPortCommand(byte[] data);

    SerialPortController setTimeOutLimit(long time);

    void destroy();

    SerialPortController setOnDataReceivedListener(OnDataReceivedListener onDataReceivedListener);
}
