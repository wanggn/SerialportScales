package com.bec.serialportlibrary;

import java.util.Arrays;

/**
 * Created by lzp on 2018/6/25 0025.
 */
@SuppressWarnings("WeakerAccess")
public class Utils {

    //校验通过
    public static final int RIGHT_STATUS = 0;

    //校验不通过
    public static final int WRONG_STATUS = 1;

    //数据长度有误
    public static final int ILLEGAL_DATA = 2;

    /**
     * @param buffer 电子秤原数据
     * @return 是否通过校验
     */
    public static int checkLegal(byte[] buffer) {

        if (buffer.length != 16) return ILLEGAL_DATA;

        byte[] temp = Arrays.copyOfRange(buffer, 2, 12);

        byte start = temp[0];

        for (int i = 1; i < temp.length; i++) {
            start = (byte) (start ^ temp[i]);
        }

        if (start == buffer[12]) {
            return RIGHT_STATUS;
        }

        return WRONG_STATUS;
    }
}
