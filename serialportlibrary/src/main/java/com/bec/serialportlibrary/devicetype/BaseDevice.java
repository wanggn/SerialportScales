package com.bec.serialportlibrary.devicetype;

import java.io.Serializable;

/**
 * Created by lzp on 2018/6/14 0014.
 */
public class BaseDevice implements Serializable {

    int baudRate = -1;

    String path = null;

    public int getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
