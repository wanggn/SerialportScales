package com.bec.serialportlibrary.devicetype;

/**
 * Created by lzp on 2018/6/14 0014.
 */
public final class DaHuaElectronicScale extends BaseDevice {

    private static final String DEFAULT_PATH = "/dev/ttyS1";

    private static final int DEFAULT_BAUD_RATE = 9600;

    public DaHuaElectronicScale() {
    }

    @Override
    public int getBaudRate() {
        return baudRate == -1 ? DEFAULT_BAUD_RATE : baudRate;
    }

    @Override
    public String getPath() {
        return path == null ? DEFAULT_PATH : path;
    }

}
