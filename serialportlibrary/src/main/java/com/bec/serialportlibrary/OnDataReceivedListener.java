package com.bec.serialportlibrary;

public interface OnDataReceivedListener {
    void onDataReceived(boolean isConnected, byte[] buffer, int size);
}